import React,{ Component } from 'react';
import $ from  "jquery";
import '../static/css/list.css';
import store from '../store.js';
import { changeUI } from '../actions/UI-action';
import { showPerson, deleteshowPerson } from '../actions/show-action';
import { addPerson, deletePerson, deleteMPerson } from '../actions/person-action';

export default class PersonList extends  Component{
    
    constructor(){
        super();
    }
    //返回
    handleBackClick(){ store.dispatch(deleteshowPerson()); store.dispatch(changeUI("INITE")); }
    //新增
    handleAddClick() {
        $(".item-add .right-info .age option:selected").attr("selected",false);
        $(".item-add .right-info .age option:first-child").prop("selected",true);
        if(!document.getElementById("preview").src){
            document.getElementById("preview").style.display = "none";
        }
        store.dispatch(changeUI("ADD_PERSON"));
    }
    //预览上传头像
    imgPreview(){
        //判断是否支持FileReader
        if (window.FileReader) {
            var reader = new FileReader();
        } else {
            alert("您的设备不支持图片预览功能，如需该功能请升级您的设备！");
        }
        //获取文件
        var fileDom = $(".content .item-add .head-pic")[0];
        var file = fileDom.files[0];
        var imageType = /^image\//;
        //是否是图片
        if (!imageType.test(file.type)) {
            alert("请选择图片！");
            return;
        }
        //读取完成
        reader.onload = function(e) {
            //获取图片dom
            var img = document.getElementById("preview");
            //图片路径设置为读取的图片
            img.src = e.target.result;
            document.getElementById("preview").style.display = "block";
        };
        reader.readAsDataURL(file);
    }
    //取消保存
    handleCancleClick(){
        //将新增页面数据重新初始化
        $("#preview").removeAttr("src");
        $(".content .item-add .head-pic").val("");
        $(".item-add .right-info .man").prop("checked", true);
        $(".item-add .right-info .woman").prop("checked", false);
        $(".content .item-add .right-info .name").val("");
        //这个不会改
        store.dispatch(changeUI("INITE"));
    }
    //保存
    handleSaveClick(){
        var pic = "", 
            name="",
            sex="",
            age="",
            onlyID  = (new Date()).valueOf(),
            t1,t2;
        clearTimeout(t1); clearTimeout(t2);
        pic = $("#preview").attr("src");
        name = $(".content .item-add .right-info .name").val().replace(/\s/g,"");
        if($(".item-add .right-info .man").is(":checked")){
            sex = "男";
        }
        else{
            sex = "女";
        }
        age = $(".item-add .right-info .age option:selected").text();
        if(name !== ""){
            store.dispatch(addPerson(pic, name, sex, age, onlyID));
            //将新增页面数据重新初始化
            $("#preview").removeAttr("src");
            $(".content .item-add .head-pic").val("");
            $(".item-add .right-info .man").prop("checked", true);
            $(".item-add .right-info .woman").prop("checked", false);
            $(".content .item-add .right-info .name").val("");
            store.dispatch(changeUI("INITE"));
        }
        else{
            $(".tips .must-name").css({"display":"block","opacity":1});
            t1 = setTimeout(function(){ 
                $(".tips .must-name").animate({"opacity":0},1000);
            },1500);
            t2 = setTimeout(function(){
                $(".tips .must-name").css({"display":"none"});
            },2500);
        }
    }
    //选择性别
    handleManClick(){
        if($(".item-add .right-info .man").is(":checked")){
            $(".item-add .right-info .man").prop("checked", true);
            $(".item-add .right-info .woman").prop("checked", false);
        }else{
            $(".item-add .right-info .man").prop("checked", false);
            $(".item-add .right-info .woman").prop("checked", true);
        }
    }
    handleWomanClick(){
        if($(".item-add .right-info .woman").is(":checked")){
            $(".item-add .right-info .woman").prop("checked", true);
            $(".item-add .right-info .man").prop("checked", false);

        }else{
            $(".item-add .right-info .woman").prop("checked", false);
            $(".item-add .right-info .man").prop("checked", true);
        }
    }
    //点击列
    handleItemClick(index){
        var obj = $('.content .list-home .info-list>li[number=' + index +'] input');
        if(obj.prop("checked")){
            obj.prop("checked",false);
        }else{
            obj.prop("checked",true);
        }
    }
    //详情按钮
    handleDetailClick(){
        var pArr = $(".content .list-home .info-list>li>input[type=checkbox]:checked"), onlyID, t1, t2;
        clearTimeout(t1); clearTimeout(t2);
        if(pArr.length === 1){
            onlyID = pArr.parent().attr("data-onlyid");
            store.dispatch(changeUI("DETAIL"));
            store.dispatch(showPerson(onlyID));
        }else{
            $(".tips .select-tip").css({"display":"block","opacity":1});
            t1 = setTimeout(function(){ 
                $(".tips .select-tip").animate({"opacity":0},1000);
            },1500);
            t2 = setTimeout(function(){
                $(".tips .select-tip").css({"display":"none"});
            },2500);
        }
    }
    //全选
    handleAllSClick(){ $(".content .list-home .info-list>li>input").prop("checked",true); }
    //取消选择
    handleAllunSClick(){ $(".content .list-home .info-list>li>input").prop("checked",false); }
    //删除所有选中
    handleDeleteSClick(){
        var pArr = $(".content .list-home .info-list>li>input[type=checkbox]:checked"), t1, t2;
        clearTimeout(t1); clearTimeout(t2);
        if(pArr.length >= 1){
            $(".tips .deletes-tip").css({"display":"block"});
        }else{
            $(".tips .select-tip").css({"display":"block","opacity":1});
            t1 = setTimeout(function(){ 
                $(".tips .select-tip").animate({"opacity":0},1000);
            },1500);
            t2 = setTimeout(function(){
                $(".tips .select-tip").css({"display":"none"});
            },2500);
        }
    }
    //取消删除
    handleDItemCancleClick(){ $(".tips .deletes-tip").css({"display":"none"}); }
    //确认删除
    handleDItemClick(){
        var deleteSet = [];
        $(".content .list-home .info-list>li>input:checked").each(function(){
            deleteSet.push($(this).parent().attr("data-onlyid"));
        });
        store.dispatch(deleteMPerson(deleteSet));
        $(".tips .deletes-tip").css({"display":"none"});
    }
    //单个删除
    handleDeleteClick(e){ store.dispatch(deletePerson($(e.target).parent().attr("data-onlyid"))); }
    
    render(){
        const nowstate = store.getState();
        var formItem = [],
            ageSelect = [],
            personInfo = [];
            
        if(nowstate.UIstate === "INITE"){
            formItem = nowstate.personSet.map((k,index)=>{
                return (
                    <li key={index} number={index} data-onlyid={k.onlyID}><input type="checkbox"/>
                        <span onClick={()=>this.handleItemClick(index)}>{k.name}</span>
                        <button onClick={(e)=>this.handleDeleteClick(e)}>删除</button>
                    </li>
                )
            })
        }
        if(nowstate.UIstate === "DETAIL"){
            var i = 0, len = nowstate.personSet.length;
            if(nowstate.showPerson){
                for(;i < len; i++){
                    if(nowstate.personSet[i].onlyID == nowstate.showPerson[0]){
                        break;
                    }
                }
                if(i < len){
                    //如果用户没上传图像则用系统默认的
                    if(nowstate.personSet[i].headPic){
                        personInfo.push( 
                        <div key={i}>
                            <img src={nowstate.personSet[i].headPic} alt="图片"></img>
                            <div className="right-info">
                                <p><span>姓名：</span><span className="person-name">{nowstate.personSet[i].name}</span></p>
                                <p><span>性别：</span><span className="person-sex">{nowstate.personSet[i].sex}</span></p>
                                <p><span>年龄：</span><span className="person-age">{nowstate.personSet[i].age}</span></p>
                            </div>
                        </div>
                        )
                        
                    }
                    else{
                        personInfo.push( 
                        <div key={i}>
                            <img src={require("../static/imges/sys-auto.jpg")} alt="图片"></img>
                            <div className="right-info">
                                <p><span>姓名：</span><span className="person-name">{nowstate.personSet[i].name}</span></p>
                                <p><span>性别：</span><span className="person-sex">{nowstate.personSet[i].sex}</span></p>
                                <p><span>年龄：</span><span className="person-age">{nowstate.personSet[i].age}</span></p>
                            </div>
                        </div>
                        )
                    }
                    
                }
            }
        }
        for(var i = 0; i < 200; i++){ ageSelect.push(<option key={i}>{i}</option>); }
        return(
        <div className="overall">
            <div className="top">
                <div className="btn-detail" style={{"display":nowstate.UIstate === "DETAIL"?"block":"none"}}>
                    <button className='btn-back btn-l' onClick={()=>this.handleBackClick()}>返回</button>
                </div>
                <div className="btns-init" style={{"display":nowstate.UIstate === "INITE"?"block":"none"}}>
                    <button className="btn-add btn-l" onClick={()=>this.handleAddClick()}>新增</button >
                    <button className="btn-detail btn-l" onClick={()=>this.handleDetailClick()}>详情</button >
                    {/* <button className="btn-alter btn-l" onClick={()=>this.handleAlterClick()}>修改</button > */}
                    <button className="btn-all btn-l" onClick={()=>this.handleAllSClick()}>全选</button >
                    <button className="btn-noall btn-l" onClick={()=>this.handleAllunSClick()}>取消选中</button >
                    <button className="btn-delete btn-l" onClick={()=>this.handleDeleteSClick()}>删除选中</button >
                </div>
                <div className="btns-add" style={{"display":nowstate.UIstate === "ADD_PERSON" ||nowstate.UIstate === "CHANGE"?"block":"none"}}>
                    <button className="btn-save btn-r" onClick={()=>this.handleSaveClick()}>保存</button >
                    <button className="btn-back btn-r" onClick={()=>this.handleCancleClick()}>取消</button >
                </div>
            </div>
            <div className="tips">
                <div className="select-tip" style={{"display":"none"}}><p>请选择一条数据</p></div>
                <div className="deletes-tip" style={{"display":"none"}}>
                    <div className="mengban"></div>
                    <div className="delete-box">
                        <p>确认要删除所选项吗</p>
                        <button className="cancle" onClick={()=>this.handleDItemCancleClick()}>取消</button>
                        <button className="confirm" onClick={()=>this.handleDItemClick()}>确认</button>
                    </div>
                </div>
                <div className="must-name" style={{"display":"none"}}><p>必须填写姓名</p></div>
            </div>
            <div className="content">
                <div className="list-home" style={{"display":nowstate.UIstate === "INITE"?"block":"none"}}>
                    <ul className="info-list">
                        {formItem}
                    </ul>
                    <div className="pagenum-box" style={{"display":"none"}}>
                        <button className="btn-pre">前一页</button>
                        <input className="page-num" type="text"/>
                        <button className="btn-next">后一页</button>
                        <button className="btn-gotot">跳转</button>
                        <span className="pageitem-num">每页条数</span>
                        <select className="select-shownum">
                            <option>10</option>
                            <option>20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                        <span className="all-data">总</span><span className="alldata-num">10</span><span className="all-datan">条数据</span><span className="allpage-num"></span ><span className="pagenum">34</span><span className="all-pagenum">页</span>
                        
                    </div>
                </div>
                <div className="item-detail" style={{"display":nowstate.UIstate === "DETAIL"?"block":"none"}}>
                    {personInfo}
                </div>
                <div className="item-change" style={{"display":nowstate.UIstate === "CHANGE"?"block":"none"}}>
                    <input type="file" className="head-pic"></input>
                    <span className="tip">请上传图片类型的文件</span>
                    <ul className="right-info">
                        <li><span>姓名：</span><input type="text" /></li>
                        <li><span>性别：</span><input type="radio"/><input  type="radio"/></li>
                        <li>
                            <span>年龄：</span>
                            <select></select>
                        </li>
                    </ul>
                </div>
                <div className="item-add" style={{"display":nowstate.UIstate === "ADD_PERSON"?"block":"none"}}>
                    <div className="head-show">
                        <div className="cross"></div>
                        <div className="verti"></div>
                    </div> 
                    <input type="file" className="head-pic" onChange={()=>this.imgPreview()}></input>
                    <img id="preview"></img>
                    <span className="tip">请上传图片类型的文件</span>
                    <ul className="right-info">
                        <li><span className="must">*</span><span>姓名：</span><input type="text" className="name" maxLength="20" required/></li>
                        <li><span className="must"></span><span>性别：</span><span>男</span><input type="radio" className="man" checked={true} onChange={function(){}} onClick={()=>this.handleManClick()}/>
                        <span>女</span><input  type="radio" className="woman"  onClick={()=>this.handleWomanClick()}/></li>
                        <li>
                            <span className="must"></span>
                            <span>年龄：</span>
                            <select className="age">{ ageSelect }</select>
                        </li>
                    </ul>
                </div>
            </div>
        </div>    
        )
    }
}