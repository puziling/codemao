import { createStore } from "redux";
import rootReducer from './reducers/index.js';

//创建存储区，只能使用reducer
//store.dispatch方法会触发 Reducer 的自动执行。为此，Store 需要知道 Reducer 函数，做法就是在生成 Store 的时候，将 Reducer 传入createStore方法。
let store = createStore(rootReducer);

export default store;