import  { ADD_TO_PERSON, DELETE_PERSON, DELETE_MANY_PERSON }  from '../actions/person-action';

const initialState = [];
//Store 收到 Action 以后，必须给出一个新的 State，这样 View 才会发生变化。这种 State 的计算过程就叫做 Reducer。
//Reducer 是一个函数，它接受 Action 和当前 State 作为参数，返回一个新的 State。
//第一个参数是当前保存在store中的数据，第二个是一个容器 b，用于更新状态
export default function(state=initialState, action) {
    switch (action.type) {
        case ADD_TO_PERSON: {
            return [...state, action.pserson];
        }
        case DELETE_PERSON:{
            return state.filter(item=>action.onlyID != item.onlyID);
        }
        case DELETE_MANY_PERSON:{
            return (function(){
                var letID = Object.assign([], action.onlyIDs);
                letID = letID.map(function(k){
                    return parseInt(k);
                });
                var nowstate = state.filter(function(item){
                    var iof = letID.indexOf(item.onlyID);
                    if(iof == -1){
                        return item;
                    }
                    else{
                        letID.splice(iof,1);
                    }
                });
                return nowstate;
            }());
        }
        default:
            return state;
    }
}