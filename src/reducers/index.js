import { combineReducers } from 'redux';
import UIchange from './UI-reducer';
import personReducer from './person-reducer';
import personDetail from './detail-reducer'

//存储着整个function
const allReducers = {
  UIstate:UIchange,
  personSet:personReducer,
  showPerson:personDetail
}
//combineReducers组合多个reducer
const rootReducer = combineReducers(allReducers);

export default rootReducer;

