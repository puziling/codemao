const initialState = [];

export default function(state=initialState, action) {
    switch(action.type){
        case "SHOW_PERSON":
            return [...state, action.onlyID];
        case "DELETE_SHOW_PERSON":
            return [];
        default:
            return state;
    }
}