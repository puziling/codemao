import React from 'react';
import ReactDOM from 'react-dom';
import store from './store.js';
import PersonList from './components/list.js'

const render = () =>ReactDOM.render(
    <div><PersonList/></div>, document.getElementById('root')
);

store.subscribe(render);
render();




