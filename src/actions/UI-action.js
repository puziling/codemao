export const UI_CHANGE = "ADD_PERSON"

export function changeUI(UIstate){
    return {
        type: UIstate,//字符串常量
        UIstate: UIstate //更新状态的数据
    }
}
