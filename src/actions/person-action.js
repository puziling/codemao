export const ADD_TO_PERSON = "ADD_TO_PERSON"
export const DELETE_PERSON = "DELETE_PERSON"
export const  DELETE_MANY_PERSON = "DELETE_MANY_PERSON";

export function addPerson(headPic, name, sex, age, onlyID){
    return {
        type: "ADD_TO_PERSON",//字符串常量
        pserson:{ headPic, name, sex, age, onlyID } //更新状态的数据
    }
}

export function deletePerson(onlyID){
    return {
        type: "DELETE_PERSON",//字符串常量
        onlyID: onlyID //更新状态的数据
    }
}

export function deleteMPerson(onlyIDs){
    return {
        type: "DELETE_MANY_PERSON",//字符串常量
        onlyIDs: onlyIDs //更新状态的数据
    }
}
